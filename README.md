* Run: Import this project into Android studio to run
* Technical decision: I'm holding the current selected birthday in the repo to cater for android system activity death
* Trade off: The above means knowledge of the state of the repo, not the ui model, or composable, is needed
* If I had more time: More tests, dealing with errors gracefully, etc
* Contact details: appsandappsandapps@icloud.com
