package com.example.chalkboardtestapp

import androidx.compose.ui.res.stringResource
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onAllNodesWithContentDescription
import androidx.compose.ui.test.onChildAt
import androidx.compose.ui.test.onChildren
import androidx.compose.ui.test.onNodeWithContentDescription
import androidx.compose.ui.test.onNodeWithTag
import androidx.compose.ui.test.onNodeWithText
import androidx.compose.ui.test.performClick
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.example.chalkboardtestapp.birthdays.BirthdaysUI

import org.junit.Test
import org.junit.runner.RunWith

import org.junit.Assert.*
import org.junit.Rule

@RunWith(AndroidJUnit4::class)
class BirthdaysUITest {

  @get:Rule
  val composeTestRule = createComposeRule()

  @Test
  fun main_screen_has_the_birthdays_header() {

    composeTestRule.setContent {
      BirthdaysUI()
    }

    composeTestRule
      .onNodeWithText("Birthdays")
      .assertExists()
  }

  @Test
  fun main_screen_to_detail_page_and_back() {

    composeTestRule.setContent { BirthdaysUI() }

    // Wait for the server
    // TODO: Use a UAT server
    composeTestRule.waitUntil(5_000) {
      composeTestRule
        .onAllNodesWithContentDescription("birthdayitem")
        .fetchSemanticsNodes().size > 1
    }

    // click on a birthday item
    composeTestRule
      .onAllNodesWithContentDescription("birthdayitem")
      .get(0)
      .performClick()

    // See if the main heading no longer exists
    composeTestRule
      .onNodeWithText("Birthdays")
      .assertDoesNotExist()

    // See if we can click on the go back button on the deail page
    composeTestRule
      .onNodeWithText("GO BACK")
      .performClick()

    // See if the main heading is there again
    composeTestRule
      .onNodeWithText("Birthdays")
      .assertExists()
  }

}