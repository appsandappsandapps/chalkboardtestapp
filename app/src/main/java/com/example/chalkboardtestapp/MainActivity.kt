package com.example.chalkboardtestapp

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.core.content.ContextCompat
import com.example.chalkboardtestapp.birthdays.BirthdaysRepository
import com.example.chalkboardtestapp.birthdays.BirthdaysUI
import com.example.chalkboardtestapp.ui.theme.ChalkboardTestAppTheme

class MainActivity : ComponentActivity() {

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    window.statusBarColor = ContextCompat.getColor(this, R.color.black)
    setContent {
      ChalkboardTestAppTheme {
        // A surface container using the 'background' color from the theme
        Surface(modifier = Modifier.fillMaxSize(), color = MaterialTheme.colorScheme.background) {
          BirthdaysUI()
        }
      }
    }
  }

}