package com.example.chalkboardtestapp

import com.example.chalkboardtestapp.birthdays.BirthdaysRepository
import com.example.chalkboardtestapp.datasource.BirthdaysDatasource
import com.example.chalkboardtestapp.datasource.BirthdaysDatasourceImp

object ServiceLocator {

  lateinit var birthdaysRepository: BirthdaysRepository
    private set

  fun init() {
    val birthdaysDatasource = BirthdaysDatasourceImp()
    birthdaysRepository = BirthdaysRepository(birthdaysDatasource)
  }
}