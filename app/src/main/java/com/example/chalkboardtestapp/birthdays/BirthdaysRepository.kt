package com.example.chalkboardtestapp.birthdays

import android.util.Log
import androidx.lifecycle.viewModelScope
import com.example.chalkboardtestapp.data.Birthday
import com.example.chalkboardtestapp.data.BirthdayDob
import com.example.chalkboardtestapp.data.BirthdayName
import com.example.chalkboardtestapp.data.BirthdayResultsAndInfo
import com.example.chalkboardtestapp.datasource.BirthdaysDatasource
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch

class BirthdaysRepository(
  private val birthdaysDatasource: BirthdaysDatasource
) {

  val birthdays = MutableStateFlow<BirthdayResultsAndInfo>(
    BirthdayResultsAndInfo(results = listOf())
  )

  val selectedBirthday = MutableStateFlow<Birthday>(
    Birthday(BirthdayName("", "", ""), BirthdayDob("", ""))
  )

  suspend fun getAllBirthdays() {
    try {
      birthdays.value = birthdaysDatasource.getBirthdays()
    } catch (e: Exception) {
      //TODO: Better deal with error
    }
  }

  suspend fun setSelectedBirthday(birthday: Birthday) {
    selectedBirthday.value = birthday
  }

}