package com.example.chalkboardtestapp.birthdays

import androidx.compose.runtime.MutableState
import com.example.chalkboardtestapp.data.Birthday
import com.example.chalkboardtestapp.data.BirthdayDob
import com.example.chalkboardtestapp.data.BirthdayName
import kotlinx.coroutines.flow.MutableStateFlow

data class BirthdaysUIState(
  val birthdays: List<Birthday> = listOf(),
  val selectedBirthday: Birthday = Birthday(
    BirthdayName("", "", ""),
    BirthdayDob("", "")
  )
)

class BirthdaysUIModel(
  val birthdaysViewModel: BirthdaysViewModel
) {

  val uiState = MutableStateFlow(BirthdaysUIState())

  fun setBirthdays(birthdays: List<Birthday>) {
    uiState.value = uiState.value.copy(
      birthdays = birthdays
    )
  }

  // We eventually store it in the repo, so it exists if the
  // activity and thus viewmodel is killed by the system
  fun selectBirthdayForDetails(birthday: Birthday) {
    birthdaysViewModel.setSelectedBirthday(birthday)
  }

  // The repo via the view model gives us this when the repo
  // has stored the repo
  fun setSelectedBirthday(birthday: Birthday) {
    uiState.value = uiState.value.copy(
      selectedBirthday = birthday
    )
  }

}