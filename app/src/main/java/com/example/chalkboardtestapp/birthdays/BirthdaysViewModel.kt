package com.example.chalkboardtestapp.birthdays

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.chalkboardtestapp.ServiceLocator
import com.example.chalkboardtestapp.data.Birthday
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class BirthdaysViewModel(
  private val birthdaysRepo: BirthdaysRepository = ServiceLocator.birthdaysRepository,
  private val ioDispatcher: CoroutineDispatcher = Dispatchers.IO,
): ViewModel() {

  val uiModel = BirthdaysUIModel(this)

  init {
    viewModelScope.launch(ioDispatcher) {
      birthdaysRepo.getAllBirthdays()
    }
    viewModelScope.launch {
      birthdaysRepo.birthdays.collect {
        uiModel.setBirthdays(it.results)
      }
    }
    viewModelScope.launch {
      birthdaysRepo.selectedBirthday.collect {
        uiModel.setSelectedBirthday(it)
      }
    }
  }

  fun setSelectedBirthday(birthday: Birthday) = viewModelScope.launch {
    birthdaysRepo.setSelectedBirthday(birthday)
  }

}