package com.example.chalkboardtestapp.birthdays.ui

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonColors
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.chalkboardtestapp.R
import com.example.chalkboardtestapp.data.Birthday
import com.example.chalkboardtestapp.ui.theme.BirthdayDetailAgeStyle
import com.example.chalkboardtestapp.ui.theme.BirthdayDetailBackButtonTextStyle
import com.example.chalkboardtestapp.ui.theme.BirthdayDetailNameStyle

@Composable
fun BirthdayDetail(
  birthday: Birthday,
  goBack: () -> Unit,
) {
  Column(
    Modifier
      .fillMaxSize(),
    horizontalAlignment = Alignment.CenterHorizontally,
  ) {
    RoundInitials(
      birthday.name.initials,
      50.sp,
      Modifier
        .padding(top = 67.dp, bottom = 70.dp)
        .size(100.dp)
    )
    Text(
      "${birthday.name.fullName}",
      Modifier.padding(bottom = 8.dp),
      style = BirthdayDetailNameStyle
    )
    Text(
      "${birthday.dob.age} years old",
      Modifier
        .padding(bottom = 109.dp),
      style = BirthdayDetailAgeStyle
    )
    Button(
      onClick = { goBack() },
      Modifier
        .fillMaxWidth()
        .padding(start = 17.dp, end = 16.dp),
      shape = RoundedCornerShape(6.dp),
      colors = ButtonDefaults.buttonColors(containerColor = Color(0xFF1C1C1C))
    ) {
      Text(
        stringResource(R.string.birthday_detail_goback),
        style = BirthdayDetailBackButtonTextStyle
      )
    }
  }
}