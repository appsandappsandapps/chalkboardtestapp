package com.example.chalkboardtestapp.birthdays.ui

import androidx.compose.material3.Divider
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color

@Composable
fun BirthdayDivider() {
  Divider(
    color = Color(0xFFE3E3E3)
  )
}