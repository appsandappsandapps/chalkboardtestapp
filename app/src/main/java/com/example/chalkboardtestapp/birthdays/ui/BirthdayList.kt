package com.example.chalkboardtestapp.birthdays.ui

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.semantics.contentDescription
import androidx.compose.ui.semantics.semantics
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.chalkboardtestapp.R
import com.example.chalkboardtestapp.data.Birthday
import com.example.chalkboardtestapp.ui.theme.BirthdayRowDateStyle
import com.example.chalkboardtestapp.ui.theme.BirthdayRowHeadingStyle

@Composable
fun BirthdayList(
  birthdays: List<Birthday>,
  onBirthdayClick: (Birthday) -> Unit,
) {
  LazyColumn(
    Modifier
      .testTag("birthdaylist")
      .semantics { contentDescription = "birthdaylist" }
  ) {
    item {
      Column {
        Text(
          stringResource(R.string.birthday_header),
          Modifier
            .fillMaxWidth()
            .padding(top = 8.dp, bottom = 8.dp),
          fontSize = 20.sp,
          fontWeight = FontWeight.Bold,
          textAlign = TextAlign.Center
        )
        BirthdayDivider()
      }
    }
    items(birthdays) {
      Column(
        Modifier
          .semantics { contentDescription = "birthdayitem" }
          .clickable { onBirthdayClick(it) }
      ) {
        Row(
          Modifier
            .padding(start = 14.dp, top = 14.dp, bottom = 14.dp),
          verticalAlignment = Alignment.CenterVertically,
        ) {
          RoundInitials(
            it.name.initials,
            18.sp
          )
          Column(
            Modifier.padding(start = 12.dp)
          ) {
            Text(
              it.name.fullName,
              Modifier
                .padding(bottom = 4.dp),
              style = BirthdayRowHeadingStyle,
            )
            Text(
              it.dob.parsedDate,
              style = BirthdayRowDateStyle,
            )
          }
        }
        BirthdayDivider()
      }
    }
  }
}
