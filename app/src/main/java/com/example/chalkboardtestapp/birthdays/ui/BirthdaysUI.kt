package com.example.chalkboardtestapp.birthdays

import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.collectAsState
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.example.chalkboardtestapp.birthdays.ui.BirthdayDetail
import com.example.chalkboardtestapp.birthdays.ui.BirthdayList
import com.example.chalkboardtestapp.data.Birthday

@Composable
fun BirthdaysUI() {
  val controller = rememberNavController()
  val birthdaysViewModel = viewModel {
    BirthdaysViewModel()
  }
  val birthdaysUIModel = birthdaysViewModel.uiModel
  val birthdaysUIState by birthdaysUIModel.uiState.collectAsState()

  val birthdays = birthdaysUIState.birthdays
  val selectedBirthday = birthdaysUIState.selectedBirthday
  val onBirthdayClick = { birthday: Birthday ->
    birthdaysUIModel.selectBirthdayForDetails(birthday)
    controller.navigate("detail")
  }

  NavHost(navController = controller,
    startDestination = "list") {
    composable("list") {
      BirthdayList(
        birthdays = birthdays,
        onBirthdayClick = onBirthdayClick,
      )
    }
    composable("detail") {
      BirthdayDetail(
        birthday = selectedBirthday,
        goBack = { controller.popBackStack() }
      )
    }
  }

}

