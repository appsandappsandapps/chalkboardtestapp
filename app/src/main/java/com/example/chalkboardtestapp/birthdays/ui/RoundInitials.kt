package com.example.chalkboardtestapp.birthdays.ui

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.dp

@Composable
fun RoundInitials(
  initials: String,
  fontSize: TextUnit,
  modifier: Modifier = Modifier,
) {
  Box(
    modifier
      .clip(RoundedCornerShape(50.dp))
      .background(Color(0xFFE3E3E3))
      .padding(vertical = 10.5.dp, horizontal = 9.dp)
  ) {
    Text(
      initials,
      Modifier.align(Alignment.Center),
      fontSize = fontSize,
      fontWeight = FontWeight.Bold
    )
  }
}