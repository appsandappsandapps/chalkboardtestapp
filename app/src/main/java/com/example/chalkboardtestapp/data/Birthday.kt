package com.example.chalkboardtestapp.data

import kotlinx.serialization.Serializable

@Serializable
data class Birthday(
  val name: BirthdayName,
  val dob: BirthdayDob
)