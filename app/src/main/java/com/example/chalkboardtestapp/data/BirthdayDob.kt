package com.example.chalkboardtestapp.data

import kotlinx.serialization.Serializable
import java.text.SimpleDateFormat

@Serializable
data class BirthdayDob (
  val age: String,
  val date: String
) {
  val parsedDate: String
    get() {
      // TOOD: Better deal with format exception
      try {
        return SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss.SSS'Z'").parse(date).let {
          SimpleDateFormat("dd/MM/yyyy").format(it)
        }
      } catch(e: Exception) {
        return "-"
      }
    }
}