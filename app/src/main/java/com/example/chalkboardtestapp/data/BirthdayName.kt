package com.example.chalkboardtestapp.data

import androidx.compose.ui.text.toUpperCase
import kotlinx.serialization.Serializable
import java.text.SimpleDateFormat

@Serializable
data class BirthdayName(
  val first: String,
  val last: String,
  val title: String,
) {
  val fullName: String
    get() = "$first $last"
  // TODO: This doesn't deal with left to right names well
  val initials: String
    get() = fullName.split(" ")
      .map { if(it.length == 0) "" else it[0]}
      .joinToString("")
      .uppercase()
      .let {
        if(it.length >= 2) it.substring(0, 2)
        else if(it.length == 1) it[0].toString()
        else ""
      }
}