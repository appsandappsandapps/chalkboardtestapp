package com.example.chalkboardtestapp.data

import kotlinx.serialization.Serializable

@Serializable
data class BirthdayResultsAndInfo(
  val results: List<Birthday> = listOf(),
)