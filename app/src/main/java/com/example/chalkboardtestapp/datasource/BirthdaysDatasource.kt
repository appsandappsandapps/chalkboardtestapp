package com.example.chalkboardtestapp.datasource

import android.util.Log
import com.example.chalkboardtestapp.GenericRESTSource
import com.example.chalkboardtestapp.data.BirthdayResultsAndInfo

interface BirthdaysDatasource {
  suspend fun getBirthdays(): BirthdayResultsAndInfo
}

class BirthdaysDatasourceImp(
  private val httpDatasource: GenericRESTSource = GenericRESTSource(),
): BirthdaysDatasource {
    override suspend fun getBirthdays(): BirthdayResultsAndInfo {
      return httpDatasource.get("https://randomuser.me/api/?results=1000&seed=chalkboard&inc=name,dob")
    }
}
