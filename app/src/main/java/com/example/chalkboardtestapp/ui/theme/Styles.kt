package com.example.chalkboardtestapp.ui.theme

import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.sp

val BirthdayRowHeadingStyle: TextStyle = TextStyle(
  color = Color(0xFF1C1C1C),
  fontSize = 16.sp,
  fontWeight = FontWeight.Bold,
)

val BirthdayRowDateStyle: TextStyle = TextStyle(
  color = Color(0xFF8B8989),
  fontSize = 16.sp
)

val BirthdayDetailNameStyle: TextStyle = TextStyle(
  color = Color(0xFF1C1C1C),
  fontSize = 32.sp,
  fontWeight = FontWeight.Bold,
)

val BirthdayDetailAgeStyle: TextStyle = TextStyle(
  color = Color(0xFF8B8989),
  fontSize = 16.sp
)

val BirthdayDetailBackButtonTextStyle: TextStyle = TextStyle(
  fontSize = 16.sp,
  fontWeight = FontWeight.Bold,
)
