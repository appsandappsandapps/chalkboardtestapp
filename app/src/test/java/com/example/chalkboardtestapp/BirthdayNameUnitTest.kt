package com.example.chalkboardtestapp

import com.example.chalkboardtestapp.data.BirthdayName
import org.junit.Test

import org.junit.Assert.*
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class BirthdayNameUnitTest {
  @Test
  fun `get name initials with three names`() {
    val name = BirthdayName(
      "first",
      "last1 last2",
      ""
    )
    assertEquals("FL", name.initials)
  }

  @Test
  fun `get name initials with one name`() {
    val name = BirthdayName(
      "first",
      "",
      ""
    )
    assertEquals("F", name.initials)
  }

  @Test
  fun `get name initials with blank string`() {
    val name = BirthdayName(
      "",
      "",
      ""
    )
    assertEquals("", "")
  }

}