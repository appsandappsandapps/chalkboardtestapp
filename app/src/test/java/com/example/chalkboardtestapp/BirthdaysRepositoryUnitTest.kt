package com.example.chalkboardtestapp

import com.example.chalkboardtestapp.birthdays.BirthdaysRepository
import com.example.chalkboardtestapp.data.Birthday
import com.example.chalkboardtestapp.data.BirthdayDob
import com.example.chalkboardtestapp.data.BirthdayName
import com.example.chalkboardtestapp.data.BirthdayResultsAndInfo
import com.example.chalkboardtestapp.datasource.BirthdaysDatasource
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collectIndexed
import kotlinx.coroutines.flow.take
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.launch
import kotlinx.coroutines.test.setMain
import org.junit.Test

import org.junit.Assert.*
import org.junit.Before
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class BirthdaysRepositoryUnitTest {

  val testDispatcher: CoroutineDispatcher = StandardTestDispatcher()

  @Before
  fun setUp() {
    Dispatchers.setMain(testDispatcher)
  }

  @Test
  fun `birthdays on repo updated on datasource call`() = runTest {

    val birthday = Birthday(
      BirthdayName("", "", ""),
      BirthdayDob("", "")
    )

    launch {
      val birthdaysRepo = setupBirthdaysRepo(birthday)
      birthdaysRepo.getAllBirthdays()
      birthdaysRepo.birthdays.take(1).collect { value ->
        assertEquals(1, value.results.size)
      }
    }.join()

  }

  private fun setupBirthdaysRepo(birthday: Birthday): BirthdaysRepository {
    val birthdaysRepo = BirthdaysRepository(
      object : BirthdaysDatasource {
        override suspend fun getBirthdays(): BirthdayResultsAndInfo =
          BirthdayResultsAndInfo(listOf(birthday))
      }
    )
    return birthdaysRepo
  }

}